    Vue.prototype.$axios = axios.create();
    Vue.options.delimiters = ['[[', ']]'];
    Vue.component('propiedad-elemento',{
        props:['elemento'],
        template:
            `<div class="htpropiedad">
                <h1>Propiedad Número [[elemento.id]]</h1>
                <p>Superficie m2: <span>[[elemento.superficie]]</span></p>
                <p>Construcción m2: <span>[[elemento.construccion]]</span></p>
                <p>Número de recamaras: <span>[[elemento.numero_recamaras]]</span></p>
                <p>Número de baños: <span>[[elemento.numero_banios]]</span></p>
                <p>Precio <span>$[[elemento.precio]]</span></p>
            </div>`
    });
    new Vue({
            el: '#app',
            data: {
                message: 'Listado de Propiedades',
                listaPropiedad:[]
            },
            mounted() {
            this.$axios.get('http://django-env.yxcgvm2w5t.us-east-2.elasticbeanstalk.com/propiedad/listar/'
            ).then((response) => {
                this.listaPropiedad = response.data;
            }).catch((err) => {
                //console.log(err);
            });
        },
    });
