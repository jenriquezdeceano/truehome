from django.views.decorators.csrf import csrf_exempt
from .serializers import PropiedadSerializer
from django.http import JsonResponse
from .models import Propiedad
from django.http import HttpResponse
from django.template import loader



@csrf_exempt
def listar_propiedades(request):
    if request.method=='GET':
        propiedades = Propiedad.objects.all()
        propiedades_a_json = PropiedadSerializer(propiedades, many=True)
        return JsonResponse(propiedades_a_json.data, safe=False)

def index(request):
    template = loader.get_template('propiedad/index.html')
    return HttpResponse(template.render({}, request))
