from django.db import models

# Create your models here.
class Propiedad(models.Model):
    superficie = models.IntegerField('Metros cuadrados de Superficie')
    construccion = models.IntegerField('Metros cuadrados de Construcción')
    numero_recamaras = models.IntegerField('Número de recamaras')
    numero_banios = models.IntegerField('Número de baños')
    precio = models.DecimalField('Precio',max_digits=13, decimal_places=2)

